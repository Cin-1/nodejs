require("dotenv").config();
var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var passport = require("./config/passport");
var session = require("express-session");
var Usuario = require("./models/usuario");
var Token = require("./models/token");
var jwt = require("jsonwebtoken");
var MongoDBStore = require("connect-mongodb-session")(session);
var newRelic = require("newrelic");

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/usuarios");
var tokenRouter = require("./routes/token");
var bicicletasRouter = require("./routes/bicicletas");
var bicicletasApiRouter = require("./routes/api/bicicletasRouter");
var usuariosApiRouter = require("./routes/api/usuariosRouter");
var authenticationApiRouter = require("./routes/api/authenticationRouter");

let store;
if (process.env.NODE_ENV === "Development") {
  store = new session.MemoryStore();
} else {
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: "sessions",
  });
  store.on("error", function (error) {
    assert.ifError(error);
    assert.ok(false);
  });
}

var app = express();

app.set("secretKey", "jwt_pwd_!!223344");

app.use(
  session({
    cookie: { maxAge: 240 * 60 * 60 * 1000 },
    store: store,
    saveUninitialized: true,
    resave: "true",
    secret: "redDeBicis",
  })
);

var mongoose = require("mongoose");
//const usuario = require('./models/usuario');
//const { token } = require('morgan');

//var mongoDB = "mongodb://localhost/red_bicicletas";
//mongodb+srv://cinthiamiriac:<password>@cluster0.7q2ua.mongodb.net/<dbname>?retryWrites=true&w=majority

var mongoDB = process.env.MONGO_URI;

mongoose.connect(mongoDB, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  useCreateIndex: true,
});
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error: "));

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, "public")));

app.get("/login", function (req, res) {
  res.render("session/login");
});

app.post("/login", function (req, res, next) {
  passport.authenticate("local", function (err, user, info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.render("session/login", { info });
    }
    req.login(user, function (err) {
      if (err) {
        return next(err);
      }
      return res.redirect("/");
    });
  })(req, res, next);
});

app.get("/logout", function (req, res) {
  req.logout();
  res.redirect("/");
});

app.get("/forgotPassword", function (req, res) {
  res.render("session/forgotPassword");
});

app.post("/forgotPassword", function (req, res) {
  Usuario.findOne({ email: req.body.email }, function (err, usuario) {
    if (!usuario) {
      return res.render("session/forgotPassword", {
        info: { message: "No existe ese e-mail para un usuario existente" },
      });
    }

    usuario.resetPassword(function (err) {
      if (err) {
        console.log("session/forgotPasswordMessage");
        return next(err);
      }
    });

    res.render("session/forgotPasswordMessage");
  });
});

app.get("/resetPassword/:token", function (req, res, next) {
  Token.findOne({ token: req.params.token }, function (err, token) {
    if (!token) {
      res.status(400).send({
        type: "Not-Verified",
        msg:
          "No existe usuario asociado al token. Verifique que no haya expirado",
      });
    }

    Usuario.findById(token._userId, function (err, user) {
      if (!user) {
        return res
          .status(400)
          .send({ msg: "No existe un usuario asociado al token" });
      }
      res.render("session/resetPassword", { errors: {}, usuario: user });
    });
  });
});

app.post("/resetPassword", function (req, res) {
  if (req.body.password != req.body.confirm_password) {
    res.render("session/resetPassword", {
      errors: {
        confirm_password: { message: "No coincide con el password ingresado" },
      },
      usuario: new Usuario(req.body.email),
    });
    return;
  }

  Usuario.findOne({ email: req.body.email }, function (err, usuario) {
    usuario.password = req.body.password;
    usuario.save(function (err) {
      if (err) {
        res.render("session/resetPassword", {
          errors: err.errors,
          usuario: new Usuario({ email: req.body.email }),
        });
      } else {
        res.redirect("/login");
      }
    });
  });
});

app.use("/", indexRouter);
app.use("/usuarios", usersRouter);
app.use("/bicicletas", loggedIn, bicicletasRouter);
app.use("/token", tokenRouter);
app.use("/api/bicicletas", validarUsuario, bicicletasApiRouter);
app.use("/api/usuarios", usuariosApiRouter);
app.use("/api/auth", authenticationApiRouter);

app.use("/google53863c3a38b1ce44", function (req, res) {
  app.sendFile("public/google53863c3a38b1ce44.html");
});

app.use("/privacy_policy", function (req, res) {
  res.sendFile("public/privacy_policy.html", { root: __dirname });
});

app.get(
  "/auth/google",
  passport.authenticate("google", {
    scope: [
      "https://www.googleapis.com/auth/plus.login",
      "https://www.googleapis.com/auth/plus.profile.emails.read",
    ],
  })
);

app.get(
  "/auth/google/callback",
  passport.authenticate("google", {
    successRedirect: "/",
    failureRedirect: "/error",
  })
);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

function loggedIn(req, res, next) {
  if (req.user) {
    next();
  } else {
    console.log("El usuario no está loggeado");
    res.redirect("/login");
  }
}

function validarUsuario(req, res, next) {
  jwt.verify(
    req.headers["x-access-token"],
    req.app.get("secretKey"),
    function (err, decoded) {
      if (err) {
        res.json({ status: "error", message: err.message, data: null });
      } else {
        req.body.userId = decoded.id;

        console.log("JWT verify: " + decoded);

        next();
      }
    }
  );
}
